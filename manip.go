package main

import (
	"bitbucket.org/zyxar/gomanip/qqwry"
	"flag"
	"fmt"
)

var (
	slice_api = flag.Bool("s", false, "use simple slice-return api")
	//compx_api = flag.Bool("x", true, "use complex self printing api")
)

func main() {
	flag.Parse()
	if flag.NArg() < 1 {
		return
	}
	reader, err := qqwry.NewReader("/opt/share/qqwry.dat")
	if err != nil {
		return
	}
	defer reader.Destroy()

	switch *slice_api {
	case true:
		res, err := reader.Query(flag.Arg(0))
		if err != nil {
			return
		}
		fmt.Printf("%v\n", res)
		return
	case false:
		reader.Queryx(flag.Arg(0))
	default:
		panic("unreachable!")
	}
}
